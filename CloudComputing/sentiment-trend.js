google.charts.load("current", {
  packages: ["corechart", "calendar"]
});
google.charts.setOnLoadCallback(start);

const viewUrl = "http://115.146.84.215:5984/tweets_2/_design/UI_View%20_5/_view/hashtags_location?group=true";

var viewDataString = localStorage.getItem("hashtags_location"); // Result from calling view
var viewData = []; // Data for chart

var primaryData = []

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

/* Visualize Google chart */
function drawPrimaryChart() {
  Highcharts.chart('sentiment-chart', {
    chart: {
      type: 'area'
    },
    title: {
      text: 'Sentiment Trend in Melbourne'
    },
    xAxis: {
      categories: monthNames,
      tickmarkPlacement: 'on',
      title: {
        enabled: false
      }
    },
    yAxis: {
      title: {
        text: 'Tweets'
      }
    },
    tooltip: {
      split: true,
      valueSuffix: ' Tweets'
    },
    plotOptions: {
      area: {
        stacking: 'normal',
        lineColor: '#666666',
        lineWidth: 1,
        marker: {
          lineWidth: 1,
          lineColor: '#666666'
        }
      }
    },
    series: primaryData
  });
}

function getSentimentScore(sentimentData) {
  var positives = sentimentData["positive"];
  var negatives = sentimentData["negative"];
  var neutrals = sentimentData["neutral"];
  return (positives - negatives) / (positives + negatives + neutrals);
}

/* Asynchronous HTTP request */
function httpGetAsync(url, callback) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      callback(xmlHttp.responseText);
  }
  xmlHttp.open("GET", url, true); // true for asynchronous
  xmlHttp.send();
}

/* Gets results from view and returns a list of array for the chart */
function parseData(data) {
  var jsonData = JSON.parse(data);
  var rows = jsonData["rows"];
  var chartData = {};

  rows.forEach(function(row) {
    var rowData = row["key"];
    var date = new Date(rowData[1], rowData[2] - 1, rowData[3]);
    var sentiment = rowData[0];

    // date.setHours(rowData[7], rowData[8]);

    /* Update information for each date of the chart data */
    if (chartData[date] == null) {
      chartData[date] = {
        'positive': 0,
        'negative': 0,
        'neutral': 0
      };
    }
    chartData[date][sentiment] += 1;
  });
  return chartData;
}

function updateYear() {
  var year = document.getElementById("year_select").value;
  var filteredData = {
    "positive": {},
    "negative": {},
    "neutral": {}
  };
  console.log("Selected: " + year);

  /* Initialize filteredData */
  for (var key in filteredData) {
    for (var monthIndex in monthNames) {
      const month = monthNames[monthIndex];
      filteredData[key][month] = 0;
    }
  }
  for (var dateString in viewData) {
    var currentData = viewData[dateString];
    var date = new Date(dateString);

    if (date.getFullYear() == year) {
      var month = monthNames[date.getMonth()];
      console.log(currentData);
      filteredData['positive'][month] += currentData['positive'];
      filteredData['negative'][month] += currentData['negative'];
      filteredData['neutral'][month] += currentData['neutral'];
    }
  }

  primaryData = convertDataToChartRows(filteredData);
  drawPrimaryChart();
}

function convertDataToChartRows(data) {
  var chartRows = [];

  for (var sentimentCategory in data) {
    var sentimentData = [];

    for (var monthIndex in monthNames) {
      var month = monthNames[monthIndex];
      sentimentData.push(data[sentimentCategory][month]);
    }

    chartRows.push({
      name: sentimentCategory,
      data: sentimentData
    });
  }
  console.log(chartRows);
  return chartRows;
}

function start() {
  httpGetAsync(viewUrl, function(result) {
    viewData = parseData(result);
    updateYear();
    drawPrimaryChart(viewData);
  });
}
