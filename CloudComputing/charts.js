google.charts.load("current", {packages:["corechart", "calendar"]});
google.charts.setOnLoadCallback(start);

const viewUrl = "http://115.146.84.215:5984/tweets_2/_design/UI_View_3/_view/auspol_view_no_loc?group=true";
var viewDataString = localStorage.getItem("auspol_view_no_loc"); // Result from calling view
var viewData = []; // Data for chart

var primaryData = []

/* Returns data in array format of [date, sentimentScore] for chart */
function convertDataToChartRows(data) {
  var chartRows = [];
  for (var key in data) {
    var date = new Date(key);
    var sentimentScore = getSentimentScore(data[key]);
    var tooltipInfo = JSON.stringify(data[key]);
    chartRows.push([date, sentimentScore, tooltipInfo]);
  }
  console.log(chartRows);
  return chartRows;
}

function convertDataToTooltipChartRows(data) {
  var chartRows = [];
  for (var key in data) {
    var date = new Date(key);
    var sentimentScore = getSentimentScore(data[key]);
    var tooltipInfo = JSON.stringify(data[key]);
    chartRows.push([date, sentimentScore, tooltipInfo]);
  }
  console.log(chartRows);
  return chartRows;
}

function drawCharts(viewData) {
  var tooltipData = getTooltipData(viewData);
  drawTooltipCharts(tooltipData, viewData);
}

/* Visualize Google chart */
function drawPrimaryChart(rows) {
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn({ type: 'date', id: 'Date' });
    dataTable.addColumn({ type: 'number', id: 'Sentiment Score' });
    // Add a new column for your tooltips.
    dataTable.addColumn({
      type: 'string',
      label: 'Tooltip Chart',
      role: 'tooltip',
      'p': {'html': true}
    });
    dataTable.addRows(rows);

    var chart = new google.visualization.Calendar(document.getElementById('calendar_basic'));
    var options = {
      title: "Analysis of tweets with #Auspol",
      // height: '2000px',
      // chartArea: {
      //   width: '100%',
      //   height: '100%'
      // },
      // calendar
      calendar: {
        cellSize: 20,
        cellColor: {
          stroke: 'red',
          strokeOpacity: 0.5,
          strokeWidth: 0.1
        },
        monthOutlineColor: {
          stroke: '#981b48',
          strokeOpacity: 0.8,
          strokeWidth: 2
        },
      },
      // legends colors
      colorAxis: {
        colors: ['#C70000', '#FFC300', '#00C751'],
        values: [-1, 0, 1]
      },
      theme: 'maximized'
    };

    chart.draw(dataTable, options);
}

/* Draw pie chart when a calendar cell is selected */
function drawTooltipCharts(tooltipData, rows) {
  var data = new google.visualization.arrayToDataTable(tooltipData);
  var view = new google.visualization.DataView(data);

  // For each row of primary data, draw a chart of its tooltip data.
  for (var i = 0; i < rows.length; i++) {
    // Set the view for each event's data
    view.setColumns([0, i + 1]);

    var hiddenDiv = document.getElementById('hidden_div');
    var tooltipChart = new google.visualization.PieChart(hiddenDiv);

    google.visualization.events.addListener(tooltipChart, 'ready', function() {
      // Get the PNG of the chart and set is as the src of an img tag.
      var tooltipImg = '<img src="' + tooltipChart.getImageURI() + '">';
      // Add the new tooltip image to your data rows.
      rows[i][2] = tooltipImg;
    });

    var dateString = rows[i][0].toDateString();
    var tooltipOptions = {
        // title: 'Sentiment Distribution',
        title: dateString,
        slices: [{color: '#00C751'}, {color: '#C70000'}, {color: '#FFC300'}]
    };
    tooltipChart.draw(view, tooltipOptions);
  }
  drawPrimaryChart(rows);
}

function getSentimentScore(sentimentData) {
  var positives = sentimentData["positive"];
  var negatives = sentimentData["negative"];
  var neutrals = sentimentData["neutral"];
  return (positives - negatives) / (positives + negatives + neutrals);
}

function getTooltipData(rows) {
   var tooltipData = [['Sentiment'], ['Positive'], ['Negative'], ['Neutral']];
   rows.forEach(function(row) {
     var sentimentData = JSON.parse(row[2]);
     tooltipData[0].push(row[0]);
     tooltipData[1].push(sentimentData['positive']);
     tooltipData[2].push(sentimentData['negative']);
     tooltipData[3].push(sentimentData['neutral']);
   });
   console.log(tooltipData);
   return tooltipData;
}

/* Asynchronous HTTP request */
function httpGetAsync(url, callback) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      callback(xmlHttp.responseText);
  }
  xmlHttp.open("GET", url, true); // true for asynchronous
  xmlHttp.send();
}

/* Gets results from view and returns a list of array for the chart */
function parseData(data) {
  var jsonData = JSON.parse(data);
  var rows = jsonData["rows"];
  var chartData = {};
  // var chartRows = [];

  rows.forEach(function(row) {
    var rowData = row["key"];
    var date = new Date(rowData[1], rowData[2] - 1, rowData[3]);
    var sentiment = rowData[0];
    var dateString = JSON.stringify(date);
    // date.setHours(rowData[7], rowData[8]);

    /* Update information for each date of the chart data */
    if (chartData[date] == null) {
      chartData[date] = {
        'positive': 0,
        'negative': 0,
        'neutral': 0
      };
    }
    chartData[date][sentiment] += 1;
  });

  return convertDataToChartRows(chartData);
}

function start() {
  if (viewDataString != null) {
    viewData = parseData(JSON.parse(viewDataString));
    drawCharts(viewData);
  } else {
    httpGetAsync(viewUrl, function(result) {
      localStorage.setItem("auspol_view_no_loc", JSON.stringify(result));
      viewData = parseData(result);
      drawCharts(viewData);
    });
  }
}
